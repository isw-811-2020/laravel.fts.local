@section('content')

    <p>
    <a href="/conversations">Back</a>
    </p>
    <h1>{{$conversations->tittle}}</h1>
    <p class="text-muted">posted by {{ $conversations->user->name }}</p>
    <div>
    {{ $conversations->body }}
    </div>

    <hr>
    @include('conversations.replies')
@endsection