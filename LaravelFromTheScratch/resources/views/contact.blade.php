
<html class="h-full">
<head>
<meta http-equiv="Content-Type"
content="text/html;charset=UTF-8">
</head>
<body class="bg-gray-100 flex items-center justify-center h-full" >
    <form method="POST" action="/contact" class="bg-white p-6 rounded shadow-md" >
        @csrf
        <div class="mb-5">
        <label for="email" class="block text-xs uppercase font-semibold mb-1"> Email Address</label>
        <input type="text" id="email" name="email" class="border px-2 py-1 text-sm w-full">
        </div>
        <button type="submit" class="bg-blue-500 py-2 text-white rounded-full text-sm w-full" >
        Send
        </button>
    </form>
</body>
<html>