<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    public function index()
    {
        return view('contact');
    }
    public function store()
    {
        request()->validate(['email'=>'required|email']);
        Mail::raw('test', function($message){
            $message->to(request('email')) ->subject('Hello There');
        });
    }
}
