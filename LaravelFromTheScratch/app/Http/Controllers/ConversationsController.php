<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConversationsController extends Controller
{
    public function index(){
        return view('conversations.index',['conversations'=>Conversatio::all()
        ]);
    }

    public function show(Conversation $conversation){
        return view('conversation.show',[
            'conversation'=>$conversation
        ]);
    }
}
