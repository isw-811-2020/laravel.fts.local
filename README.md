# Laravel From the Scratch by [Noelchelo](https://gitlab.com/isw-811-2020/laravel.fts.local)

## Steps

[1. Routing](../docs/Routing.md)

[2. Database Access](../docs/Database.md)

[3. Views](../docs/Views.md)

[4. Forms](../docs/Forms.md)

[5. Controllers Techniques](../docs/ControllerTechniques.md)

[6. Eloquent](../docs/Eloquent.md)

[7. Authentication](../docs/Authentication.md)

[8. Collections](../docs/CoreConcepts.md)

[9. Mail](../docs/Mail.md)

[10. Notifications](../docs/Notifications.md)

[11. Events](../docs/Events.md)

[12. Authorizations](../docs/Authorizations.md)