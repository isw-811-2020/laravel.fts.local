# Eloquent

## Definition
Eloquent is a implementation for working with the database. Each database table has a corresponding Model which is used to interact with that table. Models allows you to query for data in the tables, as well as insert new records into the table.

## Example
We need to  create a model,igration and controller <br/>
**php artisan make:model User** <br/>
**php artisan make:controller UserController**<br>
**php artisan make:migration create_users_table**<br>
Now we can add our methods to get whatever we want thanks to Eloquent
![Eloquent](images/eloquent.png "Eloquent")

[Home](../README.md)