# Controllers Techniques

## Definition
Laravel has a differents techniques to work with the controllers and models one of them is **Route Model Binding** basically it is to "auto-inject" instances of some model using Laravel routes.

## Example
So we need to add the route
![ControllerTe](images/controllerTe.png "ControllerTe")

Then we need to verify that the name of our model is the same as our route, because if is not the same as our route Laravel will could not find the view, model or route.
![ControllerTe1](images/controllerTe1.png "ControllerTe1")

[Home](../README.md)