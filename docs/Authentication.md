# Authentication

## Definition
Laravel makes implementing authentication very simple. In fact, almost everything is configured. The authentication configuration file is located at **config/auth.php**, which contains several well documented options for tweaking the behavior of the authentication services.<br/>
Laravel's authentication facilities are made up of "guards" and "providers". Guards define how users are authenticated for each request. For example, Laravel ships with a session guard which maintains state using session storage and cookies.
## Example

![Auth](images/auth.png "Auth")

[Home](../README.md)