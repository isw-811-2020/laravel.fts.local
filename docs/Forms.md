# Forms

## Definition
There are seven restful controller actions <br/>
**index** - should render a list of resources <br/>
**show** - should show a single resource<br/>
**create** - shows a view to create a new resource<br/>
**store** - Persist the new resource<br/>
**edit** - show a view to edit a existing resource<br/>
**update** - Persist the edited resource<br/>
**destroy** - Delete the resource<br/>
 using forms when we submit, we can be specific in which one we want to use.

 With Laravel, we can create controllers using **php artisan make:controller NameController -r**, we can write our functions here and then we should route our controllers.

## Example

![Controller](images/controller.png "Controller")

[Home](../README.md)