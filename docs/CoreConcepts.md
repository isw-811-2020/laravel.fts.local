# Core Concepts

## Collections
The Illuminate\Support\Collection class provides a fluent, convenient wrapper for working with arrays of data. For example, check out the following code. We'll use the collect helper to create a new collection instance from the array, run the strtoupper function on each element, and then remove all empty elements. <br/>
The point using Collections  is that we can get any item in our structure.

## Example

We can see all the methods Collection has and we can use it
![ColleMed](images/colleMed.png "ColleMed")

[Home](../README.md)