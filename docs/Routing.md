# Routing

## Definition

Routing is one of the essential in Laravel allows you to route all your application requests to its appropriate controller. The main and primary routes in Laravel acknowledge and accept a URI along with a closure, given that it should have to be simple and expressive way of routing, all of that with Eloquent.

## Example

![Routing](images/routing.png "Routing")

We can see how by default laravel is using Routing and Eloquent, and now we can go the site to be sure if is working on, **using php artisan serve** cmd, by dafault the output will be this http://127.0.0.1:8000 

![RoutingHome](images/routinghome.png "RoutingHome")

[Home](../README.md)