# Authorizations

## Definition
Some actions should be limited to authorized users. Perhaps only the creator of a conversation may select which reply best answered their question. If this is the case, we'll need to write the necessary authorization logic. 
Basically we are going to create roles for the different methods or actions that a user can do.
## Example

First we need to create a new controller

**php artisan make:controller ConversationsController**

then we create our views, when we finish, we can use **@can** to use a role
![Can](images/can.png "Can")

then we add the role in AuthServiceProvider

![Canro](images/canro.png "Canro")

so this is a way to create roles for our users.

[Home](../README.md)
