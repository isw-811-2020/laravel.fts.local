# Database

## Definition

With Laravel all the files are already setup, so we only need to configure our **.evn** file with the database we are using, then if we want we can make migrations to our database.

## Example

We can se **.env** file and how can be configured.

![DatabaseEnv](images/databaseenv.png "DatabaseEnv")

Then we can make migrations, Laravel already has migrations, but we can create new ones using 
**php artisan make:migration tablename** and then configure the table we already create.

![DatabaseMigra](images/databasemigra.png "DatabaseMigra")


[Home](../README.md)