# Views

## Definition
A notification may be dispatched through any number of "channels." Perhaps a particular notification should alert the user via email and through the website. Laravel provides support for sending notifications across a variety of delivery channels, including mail, SMS (via Vonage, formerly known as Nexmo), and Slack. 
## Example

First, we create a new table

**php artisan notifications:table**

**php artisan migrate**

![Noti](images/noti.png "Noti")<br>

we will use this table to save data and then send notifications, we can add auth to the notifications

![SNoti](images/sendnoti.png "SNoti")<br>


[Home](../README.md)