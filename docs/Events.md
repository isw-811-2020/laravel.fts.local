# Event

## Definition
Laravel's events provide a simple observer implementation, allowing you to subscribe and listen for various events that occur in your application. The EventServiceProvider included with your Laravel application provides a convenient place to register all of your application's event listeners.

## Example

we are doing payments, so we need to notify to user about the payment, or send sheareble coupon to user, so we need to create a event. if we have to many events, we can create another controller and we can keep it clean.

**php artisan make:event**

![Event](images/event.png "Event")

Now we have a event we can create listeners for the event

**php arrisan make:listener**
![Listener](images/listener.png "Listener")

Finally we can add the new events in our EventServices

![Events](images/events.png "Events")

[Home](../README.md)
