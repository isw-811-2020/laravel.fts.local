# Mail

## Definition
Laravel has a method to send mails with the Mail::raw() method. we can submit a form, read a provided email address from the request, and then fire off an email to the person.

## Example
First we need to create our form to send a mail <br>
![TestMail](images/testmail.png "TestMail")<br>
Then we configure our mail to send a email<br>
![ConfMail](images/confmail.png "ConfMail")<br>
Finally we write our code to send our mail<br>
![Mail](images/mail.png "Mail")<br>

[Home](../README.md)
